import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('../views/About.vue')
    },
    {
      path: '/koute',
      name: 'Koute',
      component: () => import('../views/Koute.vue')
    },
    {
      path: '/cmd',
      name: 'Commander',
      component: () => import('../views/Cmd.vue')
    },
    {
      path: '/lucide',
      name: 'Lucide',
      component: () => import('../views/Lucide.vue')
    },
  ]
})

router.beforeEach((to, from, next) => {
  if (!to.matched.length) {
    next('/');
  } else {
    document.title = to.name + " | KoteraHQ";
    next();
  }
});

export default router
